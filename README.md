# getpass

Hashed password for replacing your old password

## Notes

This repository is now **working**. However, this repository is still **in development phase**.

## How to use

Make sure the script executable

```bash
chmod +x getpass
```

Move the script into your bin folder (which is included in your PATH)

```bash
sudo mv getpass /usr/local/bin/
sudo chown ${USER}:${USER} /usr/local/bin/getpass
```

Finally, run the script from anywhere

```bash
getpass
```

Optional: you can change the salt if you need to before using it as your daily usage

```bash
nano getpass
```

## LICENSE

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png "WTFPL")

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

Copyright (C) 2018 Fatah Nur Alam Majid <fatahnuram@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.